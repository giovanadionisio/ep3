Rails.application.routes.draw do
  resources :pratos do
    resources :comentarios
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root :to => redirect('/pratos')
end

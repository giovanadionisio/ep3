class Comentario < ApplicationRecord
  belongs_to :prato
  validates :autor, :presence => true
  validates :corpo, :presence => true
end

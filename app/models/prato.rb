class Prato < ApplicationRecord
    has_attached_file :image, :styles => { :medium => "300x300>"}
    has_many :comentarios, dependent: :destroy
    validates :nome, :presence => true
    validates :descricao, :presence => true
    validates :image, :presence => true
    validates :nome_usuario, :presence => true
end

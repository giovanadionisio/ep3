class ComentariosController < ApplicationController
    def create
      @prato = Prato.find(params[:prato_id])
      @comentario = @prato.comentarios.create(com_params)
      redirect_to prato_path(@prato)
    end
    
    def destroy
      @prato = Prato.find(params[:prato_id])
      @comentario = @prato.comentarios.find(params[:id])
      @comentario.destroy
      redirect_to prato_path(@prato)
    end
    
    def com_params
      params.require(:comentario).permit(:autor, :corpo)
    end
  end

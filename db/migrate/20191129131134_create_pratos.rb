class CreatePratos < ActiveRecord::Migration[6.0]
  def change
    create_table :pratos do |t|
      t.string :nome
      t.text :descricao
      t.string :image
      t.string :nome_usuario

      t.timestamps
    end
  end
end

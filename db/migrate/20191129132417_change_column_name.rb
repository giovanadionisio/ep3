class ChangeColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :pratos, :image, :image_file_name
  end
end

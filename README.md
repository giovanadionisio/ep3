# Festival Gastrônomico FGA 
* Giovana Vitor Dionsio Santana - 18/0017659
* Denniel William Roriz Lima - 17/0161871
* Turma Carla
 ## Desenvolvimento 
 * Versão Ruby: 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]
 * **Gems utilizadas:**
 ** Rails (6.0.1)
 ** Sqlite3 (1.4)
 ** Puma (4.1)
 ** Sass-rails
 ** Webpacker (4.0)
 ** Turbolinks (5.0)
 ** Jbuilder (2.7)
 ** Bootsnap (1.4.2)
 ** Byebug
 ** Web-console (3.3.0)
 ** Listen (3.0.5), (3.2)
 ** Spring
 ** Spring-watcher-listen (2.0.0)
 ** Capybara (2.15)
 ** Selenium-webdriver
 ** Webdrivers
 ** Tzinfo-data
 ** Paperclip (3.0)
* Para a execução do programa é necessário ter o Ruby on Rails instalado no seu computador
## Instruções
Para executar o programa, digite o comando:
~~~
$ rails server
~~~
e entrar no link http://127.0.0.1:3000/ pelo seu navegador. Será exibida a página inicial no programa.



